use std::path::{Path, PathBuf};

pub trait LoadFrom: Sized {
    type Error;

    fn load_from<P>(filename: P) -> Result<Self, Self::Error> where P: AsRef<Path>, PathBuf: From<P>;
}
