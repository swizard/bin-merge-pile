use std::{fs, io};
use std::path::{Path, PathBuf};
use std::io::{BufReader, BufWriter};
use std::marker::PhantomData;
use rand::{thread_rng, Rng};
use serde::{Serialize, Deserialize};
use rmp_serde;
use rmp_serde::{Serializer, Deserializer};
use super::super::merge::{WriteTape, TapesCreator};
use super::super::save_as::SaveAs;
use super::super::load_from::LoadFrom;

#[derive(Debug)]
pub enum Error {
    CreateTemp(PathBuf, io::Error),
    OpenTemp(PathBuf, io::Error),
    Rename { from: PathBuf, to: PathBuf, error: io::Error, },
    FileNotCreated,
    Serialize(rmp_serde::encode::Error),
    Deserialize(rmp_serde::decode::Error),
}

pub struct FileTape<T> {
    filename: PathBuf,
    sink: Option<BufWriter<fs::File>>,
    _marker: PhantomData<T>,
}

impl<T> Drop for FileTape<T> {
    fn drop(&mut self) {
        if self.sink.is_some() {
            self.sink = None;
            let _ = fs::remove_file(&self.filename);
        }
    }
}

impl<'s, T> WriteTape for FileTape<T> where T: Serialize + Deserialize<'s> + PartialOrd + Send + 'static {
    type Item = T;
    type Error = Error;
    type ReadTape = FileTapeFinisher<T>;

    fn record(&mut self, value: T) -> Result<(), Error> {
        match self.sink {
            Some(ref mut sink) => {
                value.serialize(&mut Serializer::new(sink)).map_err(Error::Serialize)?;
                Ok(())
            },
            None =>
                Err(Error::FileNotCreated),
        }
    }

    fn stop(mut self) -> Result<FileTapeFinisher<T>, Error> {
        self.sink = None;
        let source = BufReader::new(match fs::File::open(&self.filename) {
            Ok(f) => f,
            Err(e) => return Err(Error::OpenTemp(self.filename.clone(), e)),
        });
        Ok(FileTapeFinisher {
            filename: self.filename.clone(),
            source: Some(source),
            keep_file: false,
            _marker: PhantomData,
        })
    }
}

pub struct FileTapeFinisher<T> {
    filename: PathBuf,
    source: Option<BufReader<fs::File>>,
    keep_file: bool,
    _marker: PhantomData<T>,
}

impl<T> Drop for FileTapeFinisher<T> {
    fn drop(&mut self) {
        self.source = None;
        if !self.keep_file {
            let _ = fs::remove_file(&self.filename);
        }
    }
}

impl<'s, T> Iterator for FileTapeFinisher<T> where T: Deserialize<'s> {
    type Item = Result<T, Error>;

    fn next(&mut self) -> Option<Result<T, Error>> {
        let result =
            if let Some(ref mut source) = self.source {
                match Deserialize::deserialize(&mut Deserializer::new(source)) {
                    Ok(value) =>
                        Some(Ok(value)),
                    Err(rmp_serde::decode::Error::InvalidMarkerRead(ref ioe)) if ioe.kind() == io::ErrorKind::UnexpectedEof =>
                        None,
                    Err(e) =>
                        Some(Err(Error::Deserialize(e))),
                }
            } else {
                None
            };
        if result.is_none() && self.source.is_some() {
            self.source = None
        }
        result
    }
}

impl<T> SaveAs for FileTapeFinisher<T> {
    type Error = Error;

    fn save_as<P>(mut self, filename: P) -> Result<(), Error> where P: AsRef<Path>, PathBuf: From<P> {
        match fs::rename(&self.filename, &filename) {
            Ok(()) => (),
            Err(e) => return Err(Error::Rename { from: self.filename.clone(), to: PathBuf::from(filename), error: e, }),
        }

        if self.source.is_some() {
            self.source = None;
            self.keep_file = true;
        }

        Ok(())
    }
}

impl<T> LoadFrom for FileTapeFinisher<T> {
    type Error = Error;

    fn load_from<P>(filename: P) -> Result<FileTapeFinisher<T>, Error> where P: AsRef<Path>, PathBuf: From<P> {
        let source = BufReader::new(match fs::File::open(&filename) {
            Ok(f) => f,
            Err(e) => return Err(Error::OpenTemp(PathBuf::from(filename), e)),
        });
        Ok(FileTapeFinisher {
            filename: PathBuf::from(filename),
            source: Some(source),
            keep_file: true,
            _marker: PhantomData,
        })
    }
}

pub struct FileTapesCreator<T> {
    temp_dir: PathBuf,
    _marker: PhantomData<T>,
}

impl<T> FileTapesCreator<T> {
    pub fn new<P: AsRef<Path>>(temp_dir: P) -> FileTapesCreator<T> {
        let mut tapes_temp_dir = PathBuf::new();
        tapes_temp_dir.push(temp_dir);
        FileTapesCreator {
            temp_dir: tapes_temp_dir,
            _marker: PhantomData,
        }
    }
}

impl<'s, T> TapesCreator for FileTapesCreator<T> where T: Serialize + Deserialize<'s> + PartialOrd + Send + 'static {
    type Item = T;
    type Error = Error;
    type Tape = FileTape<T>;

    fn make_tape(&mut self, max_len: usize) -> Result<FileTape<T>, Error> {
        let tmp_filename =
            format!("tape-bkd-file-{}-{}",
                    max_len,
                    thread_rng().gen_ascii_chars().take(10).collect::<String>());
        let mut filename = self.temp_dir.clone();
        filename.push(tmp_filename);
        let sink = BufWriter::new(match fs::File::create(&filename) {
            Ok(f) => f,
            Err(e) => return Err(Error::CreateTemp(filename.clone(), e)),
        });

        Ok(FileTape {
            filename: filename,
            sink: Some(sink),
            _marker: PhantomData,
        })
    }
}
