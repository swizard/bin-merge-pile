use std::{vec, mem};
use std::marker::PhantomData;
use super::super::merge::{WriteTape, TapesCreator};

pub enum InMemoryTape<T> {
    Empty,
    Tape1(T),
    Tape2(T, T),
    Tape3(T, T, T),
    Tape4(T, T, T, T),
    Many(Vec<T>),
}

impl<T> WriteTape for InMemoryTape<T> where T: PartialOrd + Send + 'static {
    type Item = T;
    type Error = ();
    type ReadTape = InMemoryTapeFinisher<T>;

    fn record(&mut self, value: T) -> Result<(), ()> {
        match self {
            &mut InMemoryTape::Many(ref mut vec) =>
                vec.push(value),
            other => {
                let next = match mem::replace(other, InMemoryTape::Empty) {
                    InMemoryTape::Empty => InMemoryTape::Tape1(value),
                    InMemoryTape::Tape1(a) => InMemoryTape::Tape2(a, value),
                    InMemoryTape::Tape2(a, b) => InMemoryTape::Tape3(a, b, value),
                    InMemoryTape::Tape3(a, b, c) => InMemoryTape::Tape4(a, b, c, value),
                    InMemoryTape::Tape4(a, b, c, d) => InMemoryTape::Many(vec![a, b, c, d, value]),
                    InMemoryTape::Many(..) => unreachable!(),
                };
                mem::replace(other, next);
            }
        }

        Ok(())
    }

    fn stop(self) -> Result<InMemoryTapeFinisher<T>, ()> {
        Ok(match self {
            InMemoryTape::Empty => InMemoryTapeFinisher::Depleted,
            InMemoryTape::Tape1(a) => InMemoryTapeFinisher::Left1(a),
            InMemoryTape::Tape2(a, b) => InMemoryTapeFinisher::Left2(a, b),
            InMemoryTape::Tape3(a, b, c) => InMemoryTapeFinisher::Left3(a, b, c),
            InMemoryTape::Tape4(a, b, c, d) => InMemoryTapeFinisher::Left4(a, b, c, d),
            InMemoryTape::Many(vec) => InMemoryTapeFinisher::LeftMany(vec.into_iter()),
        })
    }
}


pub enum InMemoryTapeFinisher<T> {
    Depleted,
    Left1(T),
    Left2(T, T),
    Left3(T, T, T),
    Left4(T, T, T, T),
    LeftMany(vec::IntoIter<T>),
}

impl<T> Iterator for InMemoryTapeFinisher<T> {
    type Item = Result<T, ()>;

    fn next(&mut self) -> Option<Result<T, ()>> {
        match self {
            &mut InMemoryTapeFinisher::Depleted =>
                None,
            &mut InMemoryTapeFinisher::LeftMany(ref mut iter) =>
                iter.next().map(|v| Ok(v)),
            other => {
                let (value, next) = match mem::replace(other, InMemoryTapeFinisher::Depleted) {
                    InMemoryTapeFinisher::Left1(value) =>
                        (value, InMemoryTapeFinisher::Depleted),
                    InMemoryTapeFinisher::Left2(value, a) =>
                        (value, InMemoryTapeFinisher::Left1(a)),
                    InMemoryTapeFinisher::Left3(value, a, b) =>
                        (value, InMemoryTapeFinisher::Left2(a, b)),
                    InMemoryTapeFinisher::Left4(value, a, b, c) =>
                        (value, InMemoryTapeFinisher::Left3(a, b, c)),
                    InMemoryTapeFinisher::Depleted | InMemoryTapeFinisher::LeftMany(..) =>
                        unreachable!(),
                };
                mem::replace(other, next);
                Some(Ok(value))
            }
        }
    }
}

pub struct InMemoryTapesCreator<T> {
    _marker: PhantomData<T>,
}

impl<T> InMemoryTapesCreator<T> {
    pub fn new() -> InMemoryTapesCreator<T> {
        InMemoryTapesCreator {
            _marker: PhantomData,
        }
    }
}

impl<T> TapesCreator for InMemoryTapesCreator<T> where T: PartialOrd + Send + 'static {
    type Item = T;
    type Error = ();
    type Tape = InMemoryTape<T>;

    fn make_tape(&mut self, max_len: usize) -> Result<InMemoryTape<T>, ()> {
        Ok(if max_len <= 4 {
            InMemoryTape::Empty
        } else {
            InMemoryTape::Many(Vec::with_capacity(max_len))
        })
    }
}
