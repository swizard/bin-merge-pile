use std::fs;
use std::path::{Path, PathBuf};
use std::io::BufWriter;
use serde::{Serialize, Deserialize};
use rmp_serde::Serializer;
use super::in_memory::{InMemoryTape, InMemoryTapeFinisher, InMemoryTapesCreator};
use super::file::{Error, FileTape, FileTapeFinisher, FileTapesCreator};
use super::super::merge::{WriteTape, TapesCreator};
use super::super::save_as::SaveAs;
use super::super::load_from::LoadFrom;

pub enum FileCacheTape<T> {
    Memory(InMemoryTape<T>),
    File(FileTape<T>),
}

impl<'s, T> WriteTape for FileCacheTape<T> where T: Serialize + Deserialize<'s> + PartialOrd + Send + 'static {
    type Item = T;
    type Error = Error;
    type ReadTape = FileCacheTapeFinisher<T>;

    fn record(&mut self, value: T) -> Result<(), Error> {
        match self {
            &mut FileCacheTape::Memory(ref mut tape) =>
                Ok(tape.record(value).unwrap()),
            &mut FileCacheTape::File(ref mut tape) =>
                tape.record(value),
        }
    }

    fn stop(self) -> Result<FileCacheTapeFinisher<T>, Error> {
        Ok(match self {
            FileCacheTape::Memory(tape) =>
                FileCacheTapeFinisher::Memory(tape.stop().unwrap()),
            FileCacheTape::File(tape) =>
                FileCacheTapeFinisher::File(tape.stop()?),
        })
    }
}

pub enum FileCacheTapeFinisher<T> {
    Memory(InMemoryTapeFinisher<T>),
    File(FileTapeFinisher<T>),
}

impl<'s, T> Iterator for FileCacheTapeFinisher<T> where T: Deserialize<'s> {
    type Item = Result<T, Error>;

    fn next(&mut self) -> Option<Result<T, Error>> {
        match self {
            &mut FileCacheTapeFinisher::Memory(ref mut finisher) =>
                finisher.next().map(|r| Ok(r.unwrap())),
            &mut FileCacheTapeFinisher::File(ref mut finisher) =>
                finisher.next(),
        }
    }
}

impl<T> SaveAs for FileCacheTapeFinisher<T> where T: Serialize {
    type Error = Error;

    fn save_as<P>(self, filename: P) -> Result<(), Error> where P: AsRef<Path>, PathBuf: From<P> {
        match self {
            FileCacheTapeFinisher::Memory(finisher) => {
                let mut sink = BufWriter::new(match fs::File::create(&filename) {
                    Ok(f) => f,
                    Err(e) => return Err(Error::CreateTemp(PathBuf::from(filename), e)),
                });

                let mut serializer = Serializer::new(&mut sink);
                for maybe_item in finisher {
                    try!(maybe_item.unwrap().serialize(&mut serializer).map_err(Error::Serialize));
                }
                Ok(())
            },
            FileCacheTapeFinisher::File(finisher) =>
                finisher.save_as(filename),
        }
    }
}

impl<T> LoadFrom for FileCacheTapeFinisher<T> {
    type Error = Error;

    fn load_from<P>(filename: P) -> Result<FileCacheTapeFinisher<T>, Error> where P: AsRef<Path>, PathBuf: From<P> {
        Ok(FileCacheTapeFinisher::File(try!(LoadFrom::load_from(filename))))
    }
}

pub struct FileCacheTapesCreator<T> {
    memory_creator: InMemoryTapesCreator<T>,
    file_creator: FileTapesCreator<T>,
    memory_limit_power: usize,
}

impl<T> FileCacheTapesCreator<T> {
    pub fn new<P: AsRef<Path>>(memory_limit_power: usize, temp_dir: P) -> FileCacheTapesCreator<T> {
        FileCacheTapesCreator {
            memory_creator: InMemoryTapesCreator::new(),
            file_creator: FileTapesCreator::new(temp_dir),
            memory_limit_power: memory_limit_power,
        }
    }
}

impl<'s, T> TapesCreator for FileCacheTapesCreator<T> where T: Serialize + Deserialize<'s> + PartialOrd + Send + 'static {
    type Item = T;
    type Error = Error;
    type Tape = FileCacheTape<T>;

    fn make_tape(&mut self, max_len: usize) -> Result<FileCacheTape<T>, Error> {
        if max_len <= (1 << self.memory_limit_power) {
            Ok(FileCacheTape::Memory(self.memory_creator.make_tape(max_len).unwrap()))
        } else {
            self.file_creator.make_tape(max_len).map(|r| FileCacheTape::File(r))
        }
    }
}
