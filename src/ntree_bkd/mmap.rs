use std::{io, fs, mem, ptr};
use std::io::Read;
use std::sync::Arc;
use std::ops::Deref;
use std::path::Path;
use std::os::unix::io::AsRawFd;
use rmp_serde;
use rmp_serde::Deserializer;
use serde::Deserialize;
use byteorder::{ReadBytesExt, NativeEndian};
use libc::{c_void, mmap, munmap, madvise};
use libc::{PROT_READ, MAP_PRIVATE, MAP_FAILED, MADV_WILLNEED};
use super::file_common::{MAGIC, BLOCK_TERMINATOR, Link, FileItem, Cursor};
use super::super::ntree::NTreeReader;

#[derive(Clone, Copy, Debug)]
pub enum MmapType {
    TrueMmap { madv_willneed: bool, },
    Malloc,
}

pub struct MmapReader<T> {
    carrier: Arc<Carrier>,
    root_offset: u64,
    offset: u64,
    item: (u64, Option<FileItem<T>>),
}

#[derive(Debug)]
pub enum Error {
    InvalidMagic,
    FileTooSmall,
    Open(io::Error),
    Read(io::Error),
    MmapFailed { errno: i32, },
    MadviseFailed { errno: i32, },
    Metadata(io::Error),
    ReadLink(rmp_serde::decode::Error),
    ReadHeader(io::Error),
    DeserializeItem(rmp_serde::decode::Error),
}

struct MmapSource {
    _fd: fs::File,
    map_area: *mut u8,
    map_len: usize,
}

unsafe impl Send for MmapSource {}
unsafe impl Sync for MmapSource {}

impl MmapSource {
    fn new(fd: fs::File, fd_len: usize, madv_willneed: bool) -> Result<MmapSource, Error> {
        let area = unsafe { mmap(ptr::null_mut(), fd_len, PROT_READ, MAP_PRIVATE, fd.as_raw_fd(), 0) };
        if area == MAP_FAILED {
            Err(Error::MmapFailed { errno: io::Error::last_os_error().raw_os_error().unwrap_or(-1), })
        } else {
            if madv_willneed {
                if unsafe { madvise(area, fd_len, MADV_WILLNEED) } != 0 {
                    return Err(Error::MadviseFailed { errno: io::Error::last_os_error().raw_os_error().unwrap_or(-1), })
                }
            }

            Ok(MmapSource { _fd: fd, map_area: area as *mut u8, map_len: fd_len, })
        }
    }
}

impl Drop for MmapSource {
    fn drop(&mut self) {
        unsafe { munmap(self.map_area as *mut c_void, self.map_len); }
    }
}

impl Deref for MmapSource {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        unsafe { ::std::slice::from_raw_parts(self.map_area as *const u8, self.map_len) }
    }
}

enum Carrier {
    Mmap(MmapSource),
    Malloc(Vec<u8>),
}

impl Deref for Carrier {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        match self {
            &Carrier::Mmap(ref mmap) =>
                mmap.deref(),
            &Carrier::Malloc(ref vec) =>
                vec.deref(),
        }
    }
}

impl<T> MmapReader<T> {
    pub fn new<P>(filename: P, mmap_type: MmapType) -> Result<MmapReader<T>, Error> where P: AsRef<Path> {
        let fd = fs::File::open(filename).map_err(Error::Open)?;
        let fd_len = fd.metadata().map_err(Error::Metadata)?.len() as usize;
        let header_len = mem::size_of::<u64>() * 2;
        if fd_len < header_len {
            return Err(Error::FileTooSmall)
        }

        let carrier = Arc::new(match mmap_type {
            MmapType::TrueMmap { madv_willneed: willneed, } =>
                Carrier::Mmap(MmapSource::new(fd, fd_len, willneed)?),
            MmapType::Malloc => {
                let mut vec = Vec::with_capacity(fd_len);
                let mut reader = io::BufReader::new(fd);
                reader.read_to_end(&mut vec).map_err(Error::Read)?;
                Carrier::Malloc(vec)
            }
        });

        // read header
        let root_offset = {
            let mut header = &carrier[fd_len - header_len ..];
            let root_offset = header.read_u64::<NativeEndian>().map_err(|e| Error::ReadHeader(From::from(e)))?;
            let magic = header.read_u64::<NativeEndian>().map_err(|e| Error::ReadHeader(From::from(e)))?;
            if magic != MAGIC {
                return Err(Error::InvalidMagic)
            }
            root_offset
        };

        Ok(MmapReader {
            carrier: carrier,
            root_offset: root_offset,
            offset: root_offset,
            item: (0, None),
        })
    }
}

impl<'s, T> NTreeReader for MmapReader<T> where T: Deserialize<'s> {
    type Item = T;
    type Error = Error;
    type Cursor = Cursor;

    fn root(&self) -> Self::Cursor {
        Cursor { offset: self.root_offset, jumps: 0, link: None, }
    }

    fn load<'a>(&'a mut self, &mut Cursor { offset: cursor, link: ref mut maybe_link, .. }: &mut Self::Cursor) -> Result<Option<&'a T>, Error> {
        loop {
            if cursor == 0 {
                return Ok(None)
            } else if self.item.0 == cursor {
                if let Some(ref item) = self.item.1 {
                    *maybe_link = Some(item.link);
                    return Ok(Some(&item.value))
                } else {
                    *maybe_link = None;
                    return Ok(None)
                }
            }

            if self.offset != cursor {
                self.offset = cursor;
            }

            let (item, shift) = {
                let start = self.offset as usize - 1;
                let mut cursor = io::Cursor::new(&self.carrier[start ..]);
                let item = {
                    let mut deserializer = Deserializer::new(&mut cursor);
                    let jump_offset: u64 = Deserialize::deserialize(&mut deserializer).map_err(Error::ReadLink)?;
                    if jump_offset == BLOCK_TERMINATOR {
                        None
                    } else {
                        Some(FileItem {
                            value: Deserialize::deserialize(&mut deserializer).map_err(Error::DeserializeItem)?,
                            link: Link {
                                advance_offset: 0,
                                jump_offset: jump_offset,
                            }
                        })
                    }
                };
                (item, cursor.position())
            };
            self.item = (cursor, item);
            self.offset += shift;
            if let (_, Some(ref mut item)) = self.item {
                item.link.advance_offset = self.offset;
            }
        }
    }

    fn advance(&self, Cursor { offset: cursor, jumps: jumps_count, link: maybe_link }: Self::Cursor) -> Self::Cursor {
        if let Some(Link { advance_offset: offset, .. }) = maybe_link {
            Cursor { offset: offset, jumps: jumps_count, link: None, }
        } else {
            Cursor { offset: cursor, jumps: jumps_count, link: None, }
        }
    }

    fn jump(&self, Cursor { offset: cursor, jumps: jumps_count, link: maybe_link }: Self::Cursor) -> Self::Cursor {
        if let Some(Link { jump_offset: offset, .. }) = maybe_link {
            Cursor { offset: offset, jumps: jumps_count + 1, link: None, }
        } else {
            Cursor { offset: cursor, jumps: jumps_count, link: None, }
        }
    }
}
