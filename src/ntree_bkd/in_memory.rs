use std::slice::Iter;
use super::super::ntree::{NTreeReader, NTreeWriter};

pub type Block<T> = Vec<BlockNode<T>>;
pub type Children<T> = Option<Block<T>>;

pub struct BlockNode<T> {
    item: T,
    children: Children<T>,
}

pub struct InMemory<T>(Children<T>);

impl<T> InMemory<T> {
    pub fn new() -> InMemory<T> {
        InMemory(None)
    }

    pub fn cursor<'a>(&'a self) -> InMemoryCursor<'a, T> {
        InMemoryCursor {
            root: self,
        }
    }
}

impl<T> NTreeWriter for InMemory<T> {
    type Item = T;
    type Error = ();
    type Position = Children<T>;
    type Block = Block<T>;
    type Result = InMemory<T>;

    fn empty_pos(&self) -> Children<T> {
        None
    }

    fn finish(mut self, root_block_pos: Children<T>) -> Result<InMemory<T>, ()> {
        self.0 = root_block_pos;
        Ok(self)
    }

    fn make_block(&self) -> Result<Block<T>, ()> {
        Ok(Vec::new())
    }

    fn write_item(&mut self, block: &mut Block<T>, item: T, child_block_pos: Children<T>) -> Result<(), ()> {
        block.push(BlockNode {
            item: item,
            children: child_block_pos,
        });
        Ok(())
    }

    fn flush_block(&mut self, block: Block<T>) -> Result<Children<T>, ()> {
        Ok(if block.is_empty() {
            None
        } else {
            Some(block)
        })
    }
}

pub struct InMemoryCur<'o, T: 'o> {
    value: Option<&'o BlockNode<T>>,
    iter: Option<Iter<'o, BlockNode<T>>>,
}

impl<'o, T> PartialEq for InMemoryCur<'o, T> {
    fn eq(&self, _other: &Self) -> bool {
        true
    }
}

impl<'o, T> PartialOrd for InMemoryCur<'o, T> {
    fn partial_cmp(&self, _other: &Self) -> Option<::std::cmp::Ordering> {
        None
    }
}

pub struct InMemoryCursor<'o, T: 'o> {
    root: &'o InMemory<T>,
}

impl<'o, T> NTreeReader for InMemoryCursor<'o, T> {
    type Item = T;
    type Error = ();
    type Cursor = InMemoryCur<'o, T>;

    fn root(&self) -> Self::Cursor {
        let mut iter = self.root.0.as_ref().map(|children| children.iter());
        let value = iter.as_mut().map(|i| i.next()).unwrap_or(None);
        InMemoryCur { value: value, iter: iter, }
    }

    fn load<'a>(&'a mut self, cursor: &mut Self::Cursor) -> Result<Option<&'a T>, ()> {
        Ok(cursor.value.map(|n| &n.item))
    }

    fn advance(&self, mut cursor: Self::Cursor) -> Self::Cursor {
        let value = cursor.iter.as_mut().map(|i| i.next()).unwrap_or(None);
        InMemoryCur { value: value, iter: cursor.iter, }
    }

    fn jump(&self, cursor: Self::Cursor) -> Self::Cursor {
        let mut iter = cursor.value.and_then(|&BlockNode { children: ref ch, .. }| ch.as_ref().map(|children| children.iter()));
        let value = iter.as_mut().map(|i| i.next()).unwrap_or(None);
        InMemoryCur { value: value, iter: iter, }
    }
}
