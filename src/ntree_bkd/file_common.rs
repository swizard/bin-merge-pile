use std::cmp::{PartialEq, PartialOrd, Ordering};

pub const MAGIC: u64 = 0x7d58fb33d2615d7e;
pub const BLOCK_TERMINATOR: u64 = 0xffffffffffffffff;

#[derive(Clone, Copy)]
pub struct Link {
    pub advance_offset: u64,
    pub jump_offset: u64,
}

pub struct FileItem<T> {
    pub value: T,
    pub link: Link,
}

pub struct Cursor {
    pub offset: u64,
    pub jumps: usize,
    pub link: Option<Link>,
}

impl PartialEq for Cursor {
    fn eq(&self, other: &Self) -> bool {
        self.jumps.eq(&other.jumps) && self.offset.eq(&other.offset)
    }
}

impl PartialOrd for Cursor {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self.jumps.partial_cmp(&other.jumps) {
            Some(Ordering::Less) => Some(Ordering::Greater),
            Some(Ordering::Greater) => Some(Ordering::Less),
            Some(Ordering::Equal) | None =>
                other.offset.partial_cmp(&self.offset),
        }
    }
}
