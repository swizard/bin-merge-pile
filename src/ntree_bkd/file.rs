use std::{io, fs, mem};
use std::io::{Seek, Write};
use std::path::Path;
use std::convert::From;
use std::marker::PhantomData;
use serde::{Serialize, Deserialize};
use rmp_serde;
use rmp_serde::{Serializer, Deserializer};
use byteorder::{ReadBytesExt, WriteBytesExt, NativeEndian};
use super::file_common::{MAGIC, BLOCK_TERMINATOR, Link, FileItem, Cursor};
use super::super::ntree::{NTreeReader, NTreeWriter};

#[derive(Debug)]
pub enum Error {
    InvalidMagic,
    FileTooSmall,
    Open(io::Error),
    Create(io::Error),
    Metadata(io::Error),
    ReadLink(rmp_serde::decode::Error),
    ReadHeader(io::Error),
    DeserializeItem(rmp_serde::decode::Error),
    SerializeLink(rmp_serde::encode::Error),
    SerializeBlock(rmp_serde::encode::Error),
    WriteBlock(io::Error),
    WriteHeader(io::Error),
    Seek(io::Error),
}

pub struct FileWriter<T> {
    fd: io::BufWriter<fs::File>,
    _marker: PhantomData<T>,
}

impl<T> FileWriter<T> {
    pub fn new<P>(filename: P) -> Result<FileWriter<T>, Error> where P: AsRef<Path> {
        let fd = fs::File::create(filename).map_err(Error::Create)?;
        Ok(FileWriter {
            fd: io::BufWriter::new(fd),
            _marker: PhantomData,
        })
    }
}

fn shift_offset(offset: Option<u64>) -> u64 {
    offset.map(|v| v + 1).unwrap_or(0)
}

impl<T> NTreeWriter for FileWriter<T> where T: Serialize {
    type Item = T;
    type Error = Error;
    type Position = Option<u64>;
    type Block = Vec<u8>;
    type Result = ();

    fn empty_pos(&self) -> Option<u64> {
        None
    }

    fn make_block(&self) -> Result<Vec<u8>, Error> {
        Ok(Vec::new())
    }

    fn write_item(&mut self, block: &mut Vec<u8>, item: T, child_block_pos: Option<u64>) -> Result<(), Error> {
        let mut serializer = Serializer::new(block);
        shift_offset(child_block_pos).serialize(&mut serializer).map_err(Error::SerializeLink)?;
        item.serialize(&mut serializer).map_err(Error::SerializeBlock)?;
        Ok(())
    }

    fn flush_block(&mut self, mut block: Vec<u8>) -> Result<Option<u64>, Error> {
        BLOCK_TERMINATOR.serialize(&mut Serializer::new(&mut block)).map_err(Error::SerializeBlock)?;
        let offset = self.fd.seek(io::SeekFrom::Current(0)).map_err(Error::Seek)?;
        self.fd.write_all(&block).map_err(Error::WriteBlock)?;
        Ok(Some(offset))
    }

    fn finish(mut self, root_block_pos: Option<u64>) -> Result<(), Error> {
        self.fd.write_u64::<NativeEndian>(shift_offset(root_block_pos)).map_err(|e| Error::WriteHeader(From::from(e)))?;
        self.fd.write_u64::<NativeEndian>(MAGIC).map_err(|e| Error::WriteHeader(From::from(e)))?;
        Ok(())
    }
}

pub struct FileReader<T> {
    fd: io::BufReader<fs::File>,
    root_offset: u64,
    offset: u64,
    item: (u64, Option<FileItem<T>>),
}

impl<T> FileReader<T> {
    pub fn new<P>(filename: P) -> Result<FileReader<T>, Error> where P: AsRef<Path> {
        let mut fd = fs::File::open(filename).map_err(Error::Open)?;
        let fd_len = fd.metadata().map_err(Error::Metadata)?.len();
        let header_len = mem::size_of::<u64>() as u64 * 2;
        if fd_len < header_len {
            return Err(Error::FileTooSmall)
        }
        // read header
        fd.seek(io::SeekFrom::Start(fd_len - header_len)).map_err(Error::Seek)?;
        let root_offset = fd.read_u64::<NativeEndian>().map_err(|e| Error::ReadHeader(From::from(e)))?;
        let magic = fd.read_u64::<NativeEndian>().map_err(|e| Error::ReadHeader(From::from(e)))?;
        if magic != MAGIC {
            return Err(Error::InvalidMagic)
        }

        let mut fd_buf = io::BufReader::new(fd);
        if root_offset > 0 {
            fd_buf.seek(io::SeekFrom::Start(root_offset - 1)).map_err(Error::Seek)?;
        }

        Ok(FileReader {
            fd: fd_buf,
            root_offset: root_offset,
            offset: root_offset,
            item: (0, None),
        })
    }
}

impl<'s, T> NTreeReader for FileReader<T> where T: Deserialize<'s> {
    type Item = T;
    type Error = Error;
    type Cursor = Cursor;

    fn root(&self) -> Self::Cursor {
        Cursor { offset: self.root_offset, jumps: 0, link: None, }
    }

    fn load<'a>(
        &'a mut self,
        &mut Cursor { offset: cursor, link: ref mut maybe_link, .. }: &mut Self::Cursor
    )
        -> Result<Option<&'a T>, Error>
    {
        loop {
            if cursor == 0 {
                return Ok(None)
            } else if self.item.0 == cursor {
                if let Some(ref item) = self.item.1 {
                    *maybe_link = Some(item.link);
                    return Ok(Some(&item.value))
                } else {
                    *maybe_link = None;
                    return Ok(None)
                }
            }

            if self.offset != cursor {
                if cursor > 0 {
                    self.fd.seek(io::SeekFrom::Start(cursor - 1)).map_err(Error::Seek)?;
                }
                self.offset = cursor;
            }

            let item = {
                let mut deserializer = Deserializer::new(&mut self.fd);
                let jump_offset = Deserialize::deserialize(&mut deserializer).map_err(Error::ReadLink)?;
                if jump_offset == BLOCK_TERMINATOR {
                    None
                } else {
                    let value = Deserialize::deserialize(&mut deserializer).map_err(Error::DeserializeItem)?;
                    Some(FileItem {
                        value: value,
                        link: Link {
                            advance_offset: 0,
                            jump_offset: jump_offset,
                        },
                    })
                }
            };
            self.item = (cursor, item);
            self.offset = self.fd.seek(io::SeekFrom::Current(0)).map_err(Error::Seek)? + 1;
            if let (_, Some(ref mut item)) = self.item {
                item.link.advance_offset = self.offset;
            }
        }
    }

    fn advance(&self, Cursor { offset: cursor, jumps: jumps_count, link: maybe_link }: Self::Cursor) -> Self::Cursor {
        if let Some(Link { advance_offset: offset, .. }) = maybe_link {
            Cursor { offset: offset, jumps: jumps_count, link: None, }
        } else {
            Cursor { offset: cursor, jumps: jumps_count, link: None, }
        }
    }

    fn jump(&self, Cursor { offset: cursor, jumps: jumps_count, link: maybe_link }: Self::Cursor) -> Self::Cursor {
        if let Some(Link { jump_offset: offset, .. }) = maybe_link {
            Cursor { offset: offset, jumps: jumps_count + 1, link: None, }
        } else {
            Cursor { offset: cursor, jumps: jumps_count, link: None, }
        }
    }
}
