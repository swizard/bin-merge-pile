use std::{mem, thread};
use std::sync::{Arc, Mutex, Condvar};
use std::iter::Iterator;

pub trait WriteTape {
    type Item;
    type Error;
    type ReadTape: Iterator<Item = Result<Self::Item, Self::Error>> + Send + 'static;

    fn record(&mut self, value: Self::Item) -> Result<(), Self::Error>;
    fn stop(self) -> Result<Self::ReadTape, Self::Error>;
}

pub trait TapesCreator {
    type Item: PartialOrd;
    type Error: Send + 'static;
    type Tape: WriteTape<Item = Self::Item, Error = Self::Error> + Send + 'static;

    fn make_tape(&mut self, max_len: usize) -> Result<Self::Tape, Self::Error>;
}

enum TapeSlot<RT, E> {
    Vacant,
    Occupied(RT, usize),
    InProgress(Arc<Mutex<Option<Result<(RT, usize), E>>>>, Arc<Condvar>),
}

#[derive(Clone, Copy)]
pub enum ParallelConfig {
    SingleThread,
    MultiThread { max_slaves: usize, spawn_trigger_power: usize, },
}

pub struct BinMergeTape<TC> where TC: TapesCreator {
    tapes_creator: TC,
    tapes: Vec<TapeSlot<<TC::Tape as WriteTape>::ReadTape, TC::Error>>,
    slaves_count: Arc<Mutex<usize>>,
    condvar: Arc<Condvar>,
    pconfig: ParallelConfig,
}

impl<TC> BinMergeTape<TC> where TC: TapesCreator {
    fn sync(&mut self) {
        // wait for all slave threads to finish
        match self.slaves_count.lock() {
            Ok(mut slaves_count) =>
                while *slaves_count > 0 {
                    slaves_count = self.condvar.wait(slaves_count).unwrap();
                },
            Err(poisoned) =>
                *poisoned.into_inner() -= 1,
        }
    }

    fn maybe_inc_slaves_count(&self, limit: usize) -> bool {
        let mut slaves_count = match self.slaves_count.lock() {
            Ok(guard) => guard,
            Err(poisoned) => poisoned.into_inner(),
        };
        if *slaves_count < limit {
            *slaves_count += 1;
            true
        } else {
            false
        }
    }
}

impl<TC> BinMergeTape<TC> where TC: TapesCreator {
    pub fn new(tapes_creator: TC) -> BinMergeTape<TC> {
        BinMergeTape::with_params(tapes_creator, ParallelConfig::SingleThread)
    }

    pub fn with_params(tapes_creator: TC, pconfig: ParallelConfig) -> BinMergeTape<TC> {
        BinMergeTape {
            tapes_creator: tapes_creator,
            tapes: Vec::new(),
            slaves_count: Arc::new(Mutex::new(0)),
            condvar: Arc::new(Condvar::new()),
            pconfig: pconfig,
        }
    }

    pub fn add(&mut self, value: TC::Item) -> Result<(), TC::Error> {
        let mut tape = self.tapes_creator.make_tape(1)?;
        tape.record(value)?;
        self.add_tape(tape.stop()?, 1)
    }

    pub fn add_tape(&mut self, mut tape: <TC::Tape as WriteTape>::ReadTape, mut tape_len: usize) -> Result<(), TC::Error> {
        loop {
            let power = bin_power_for(tape_len);

            // ensure we have a tape with power required
            while self.tapes.len() <= power {
                self.tapes.push(TapeSlot::Vacant);
            }

            match mem::replace(&mut self.tapes[power], TapeSlot::Vacant) {
                TapeSlot::Vacant => {
                    self.tapes[power] = TapeSlot::Occupied(tape, tape_len);
                    return Ok(());
                },
                TapeSlot::Occupied(existing_tape, existing_tape_len) => {
                    let target_tape =
                        self.tapes_creator.make_tape(tape_len + existing_tape_len)?;
                    match self.pconfig {
                        ParallelConfig::MultiThread {
                            max_slaves: slaves_limit,
                            spawn_trigger_power: spawn_power,
                        } if power >= spawn_power && self.maybe_inc_slaves_count(slaves_limit) => {
                            let result_slot = Arc::new(Mutex::new(None));
                            let result_condvar = Arc::new(Condvar::new());
                            self.tapes[power] = TapeSlot::InProgress(result_slot.clone(), result_condvar.clone());
                            let thread_slaves_count = self.slaves_count.clone();
                            let thread_condvar = self.condvar.clone();
                            thread::spawn(move || {
                                let merge_result = merge_tapes(tape, existing_tape, target_tape);
                                let mut result_guard = result_slot.lock().unwrap();
                                mem::replace(&mut *result_guard, Some(merge_result));
                                result_condvar.notify_one();
                                let mut slaves_count_lock = thread_slaves_count.lock().unwrap();
                                *slaves_count_lock -= 1;
                                thread_condvar.notify_one();
                            });
                            return Ok(());
                        },
                        ParallelConfig::SingleThread | ParallelConfig::MultiThread { .. } => {
                            let (merged_tape, merged_tape_len) = merge_tapes(tape, existing_tape, target_tape)?;
                            tape = merged_tape;
                            tape_len = merged_tape_len;
                        },
                    }
                },
                TapeSlot::InProgress(result_slot, result_condvar) => {
                    let mut merged_result_guard = result_slot.lock().unwrap();
                    while merged_result_guard.is_none() {
                        merged_result_guard = result_condvar.wait(merged_result_guard).unwrap();
                    }
                    let merged_result = &mut *merged_result_guard;
                    let (merged_tape, merged_tape_len) = merged_result.take().unwrap()?;
                    self.tapes[power] = TapeSlot::Occupied(tape, tape_len);
                    tape = merged_tape;
                    tape_len = merged_tape_len;
                },
            }
        }
    }

    pub fn finish(mut self) -> Result<Option<(<TC::Tape as WriteTape>::ReadTape, usize)>, TC::Error> {
        let mut current = None;
        for slot in self.tapes.iter_mut() {
            let merge_tape = match mem::replace(slot, TapeSlot::Vacant) {
                TapeSlot::Vacant =>
                    None,
                TapeSlot::Occupied(existing_tape, existing_tape_len) =>
                    Some((existing_tape, existing_tape_len)),
                TapeSlot::InProgress(result_slot, result_condvar) => {
                    let mut merged_result_guard = result_slot.lock().unwrap();
                    while merged_result_guard.is_none() {
                        merged_result_guard = result_condvar.wait(merged_result_guard).unwrap();
                    }
                    let merged_result = &mut *merged_result_guard;
                    Some(merged_result.take().unwrap()?)
                },
            };

            current = match (current, merge_tape) {
                (None, None) => None,
                (None, Some(tape_to_merge)) => Some(tape_to_merge),
                (Some(tape), None) => Some(tape),
                (Some((tape, tape_len)), Some((tape_to_merge, tape_to_merge_len))) => {
                    let target = self.tapes_creator.make_tape(tape_len + tape_to_merge_len)?;
                    Some(merge_tapes(tape, tape_to_merge, target)?)
                },
            };
        }

        Ok(current)
    }
}

impl<TC> Drop for BinMergeTape<TC> where TC: TapesCreator {
    fn drop(&mut self) {
        self.sync()
    }
}

fn bin_power_for(len: usize) -> usize {
    (len as f64).log2().ceil() as usize
}

fn merge_tapes<Item, Error, Tape>(
    mut src_a: <Tape as WriteTape>::ReadTape,
    mut src_b: <Tape as WriteTape>::ReadTape,
    mut target: Tape
) -> Result<(<Tape as WriteTape>::ReadTape, usize), Error>
    where Item: PartialOrd, Tape: WriteTape<Item = Item, Error = Error>
{
    let mut total = 0;
    let (mut elem_a, mut elem_b) = (src_a.next(), src_b.next());
    loop {
        let (next_elem_a, next_elem_b) =
            match (elem_a, elem_b) {
                (None, None) =>
                    return Ok((target.stop()?, total)),
                (Some(Err(e)), _) =>
                    return Err(e),
                (_, Some(Err(e))) =>
                    return Err(e),
                (Some(Ok(item)), None) => {
                    target.record(item)?;
                    total += 1;
                    (src_a.next(), None)
                },
                (None, Some(Ok(item))) => {
                    target.record(item)?;
                    total += 1;
                    (None, src_b.next())
                },
                (Some(Ok(item_a)), Some(Ok(item_b))) => {
                    if item_a < item_b {
                        target.record(item_a)?;
                        total += 1;
                        (src_a.next(), Some(Ok(item_b)))
                    } else {
                        target.record(item_b)?;
                        total += 1;
                        (Some(Ok(item_a)), src_b.next())
                    }
                },
            };
        elem_a = next_elem_a;
        elem_b = next_elem_b;
    }
}
