#![cfg_attr(test, feature(test))]
extern crate libc;
extern crate rand;
extern crate serde;
extern crate rmp;
extern crate rmp_serde;
extern crate byteorder;

pub mod merge;
pub mod reduce;
pub mod bkd;
pub mod ntree;
pub mod ntree_bkd;
pub mod save_as;
pub mod load_from;

#[cfg(test)]
mod test {
    extern crate test;
    use std::fmt::Debug;
    use rand::{thread_rng, Rng};

    pub trait Sampler: Clone + Debug {
        fn make_sample(total: usize) -> Vec<Self>;
    }

    impl Sampler for i32 {
        fn make_sample(total: usize) -> Vec<i32> {
            thread_rng().gen_iter().take(total).collect()
        }
    }

    impl Sampler for String {
        fn make_sample(total: usize) -> Vec<String> {
            let mut rng = thread_rng();
            (0 .. total)
                .map(|_| {
                    let len = (rng.gen::<usize>() % 14) + 2;
                    rng.gen_ascii_chars().take(len).collect::<String>()
                })
                .collect()
        }
    }

    mod mod_test {
        use std::cmp::Ordering;
        use super::Sampler;
        use super::super::bkd::{in_memory, file, file_cache};
        use super::super::merge::{TapesCreator, BinMergeTape, ParallelConfig};
        use super::super::reduce::{Reducer, ReducerTapesCreator};
        use super::super::ntree::{NTreeReader, NTreeWriter};
        use super::super::ntree_bkd;

        fn test_merge_tape<T, PC>(mut merge_tape: BinMergeTape<PC>, total: usize) -> Result<(), PC::Error>
            where T: Sampler + Ord, PC: TapesCreator<Item = T>
        {
            let mut sample = T::make_sample(total);

            for value in sample.iter().cloned() {
                merge_tape.add(value)?;
            }

            sample.sort();
            let (mut src_tape, _) = merge_tape.finish()?.unwrap();
            let mut src_sample = sample.into_iter();

            loop {
                match (src_sample.next(), src_tape.next()) {
                    (None, None) =>
                        return Ok(()),
                    (Some(item_sample), Some(Ok(item_tape))) =>
                        assert_eq!(item_sample, item_tape),
                    _ =>
                        panic!("unexpected (sample, item)"),
                }
            }
        }

        #[test]
        fn in_memory_bin_merge_tape_i32_singlethread() {
            let merge_tape = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
            test_merge_tape::<i32, _>(merge_tape, 256 * 1024).unwrap();
        }

        #[test]
        fn in_memory_bin_merge_tape_str_singlethread() {
            let merge_tape = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
            test_merge_tape::<String, _>(merge_tape, 256 * 1024).unwrap();
        }

        #[test]
        fn in_memory_bin_merge_tape_i32_multithread() {
            let merge_tape = BinMergeTape::with_params(
                in_memory::InMemoryTapesCreator::new(),
                ParallelConfig::MultiThread { max_slaves: 4, spawn_trigger_power: 14, });
            test_merge_tape::<i32, _>(merge_tape, 256 * 1024).unwrap();
        }

        #[test]
        fn in_memory_bin_merge_tape_str_multithread() {
            let merge_tape = BinMergeTape::with_params(
                in_memory::InMemoryTapesCreator::new(),
                ParallelConfig::MultiThread { max_slaves: 4, spawn_trigger_power: 14, });
            test_merge_tape::<String, _>(merge_tape, 256 * 1024).unwrap();
        }

        #[test]
        fn file_bin_merge_tape_i32_singlethread() {
            let merge_tape = BinMergeTape::new(file::FileTapesCreator::new("/tmp"));
            test_merge_tape::<i32, _>(merge_tape, 128).unwrap();
        }

        #[test]
        fn file_bin_merge_tape_str_singlethread() {
            let merge_tape = BinMergeTape::new(file::FileTapesCreator::new("/tmp"));
            test_merge_tape::<String, _>(merge_tape, 128).unwrap();
        }

        #[test]
        fn file_bin_merge_tape_i32_multithread() {
            let merge_tape = BinMergeTape::with_params(
                file::FileTapesCreator::new("/tmp"),
                ParallelConfig::MultiThread { max_slaves: 4, spawn_trigger_power: 6, });
            test_merge_tape::<i32, _>(merge_tape, 128).unwrap();
        }

        #[test]
        fn file_bin_merge_tape_str_multithread() {
            let merge_tape = BinMergeTape::with_params(
                file::FileTapesCreator::new("/tmp"),
                ParallelConfig::MultiThread { max_slaves: 4, spawn_trigger_power: 6, });
            test_merge_tape::<String, _>(merge_tape, 128).unwrap();
        }

        #[test]
        fn file_cache_bin_merge_tape_i32_singlethread() {
            let merge_tape = BinMergeTape::new(file_cache::FileCacheTapesCreator::new(14, "/tmp"));
            test_merge_tape::<i32, _>(merge_tape, 256 * 1024).unwrap();
        }

        #[test]
        fn file_cache_bin_merge_tape_str_singlethread() {
            let merge_tape = BinMergeTape::new(file_cache::FileCacheTapesCreator::new(14, "/tmp"));
            test_merge_tape::<String, _>(merge_tape, 256 * 1024).unwrap();
        }

        #[test]
        fn file_cache_bin_merge_tape_i32_multithread() {
            let merge_tape = BinMergeTape::with_params(
                file_cache::FileCacheTapesCreator::new(14, "/tmp"),
                ParallelConfig::MultiThread { max_slaves: 4, spawn_trigger_power: 13, });
            test_merge_tape::<i32, _>(merge_tape, 256 * 1024).unwrap();
        }

        #[test]
        fn file_cache_bin_merge_tape_str_multithread() {
            let merge_tape = BinMergeTape::with_params(
                file_cache::FileCacheTapesCreator::new(14, "/tmp"),
                ParallelConfig::MultiThread { max_slaves: 4, spawn_trigger_power: 13, });
            test_merge_tape::<String, _>(merge_tape, 256 * 1024).unwrap();
        }

        struct Entry(String, i32);
        impl PartialEq for Entry { fn eq(&self, other: &Entry) -> bool { self.0.eq(&other.0) } }
        impl PartialOrd for Entry { fn partial_cmp(&self, other: &Entry) -> Option<Ordering> { self.0.partial_cmp(&other.0) } }

        fn reducer_simple_test<R, E>(reducer: R) where R: Reducer<Entry, Error = E>, E: ::std::fmt::Debug + Send + 'static {
            let mut merge_tape = BinMergeTape::new(ReducerTapesCreator::new(in_memory::InMemoryTapesCreator::new(), reducer));

            merge_tape.add(Entry("string a".to_owned(), 1)).unwrap();
            merge_tape.add(Entry("string b".to_owned(), 1)).unwrap();
            merge_tape.add(Entry("string c".to_owned(), 1)).unwrap();
            merge_tape.add(Entry("string a".to_owned(), 1)).unwrap();
            merge_tape.add(Entry("string b".to_owned(), 1)).unwrap();
            merge_tape.add(Entry("string a".to_owned(), 1)).unwrap();

            let results: Vec<_> = merge_tape.finish().unwrap().unwrap().0.map(|r| r.unwrap()).map(|Entry(_, c)| c).collect();
            assert_eq!(results, &[3, 2, 1]);
        }

        #[test]
        fn reducer_fn() {
            reducer_simple_test(|&mut Entry(_, ref mut e_cnt): &mut Entry, Entry(_, i_cnt): Entry| {
                *e_cnt += i_cnt;
                Ok::<(), ()>(())
            });
        }

        #[test]
        fn reducer_trait() {
            struct EntriesSum;
            impl Reducer<Entry> for EntriesSum {
                type Error = ();

                fn reduce(&self, &mut Entry(_, ref mut e_cnt): &mut Entry, Entry(_, i_cnt): Entry) -> Result<(), Self::Error> {
                    *e_cnt += i_cnt;
                    Ok(())
                }
            }
            reducer_simple_test(EntriesSum);
        }

        #[test]
        fn merge_tapes() {
            let mut tape_a = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
            let mut tape_b = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
            let mut tape_c = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());

            tape_a.add(1i32).unwrap();
            tape_a.add(4i32).unwrap();
            tape_b.add(2i32).unwrap();
            tape_b.add(5i32).unwrap();
            tape_b.add(7i32).unwrap();
            tape_c.add(3i32).unwrap();
            tape_c.add(6i32).unwrap();
            let (res_a, len_a) = tape_a.finish().unwrap().unwrap();
            let (res_b, len_b) = tape_b.finish().unwrap().unwrap();
            let (res_c, len_c) = tape_c.finish().unwrap().unwrap();

            let mut tape_r = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
            tape_r.add_tape(res_a, len_a).unwrap();
            tape_r.add_tape(res_b, len_b).unwrap();
            tape_r.add_tape(res_c, len_c).unwrap();
            let (res_r, len_r) = tape_r.finish().unwrap().unwrap();
            assert_eq!(len_r, 7);
            let elements_r: Vec<_> = res_r.map(|r| r.unwrap()).collect();
            assert_eq!(&elements_r[..], &[1, 2, 3, 4, 5, 6, 7]);
        }

        fn fill_tape_and_check_table<T, E, PC>(merge_tape: &mut BinMergeTape<PC>, total: usize) -> Vec<T> where
            T: Ord + Clone + Sampler + ::std::hash::Hash + Send + 'static,
            E: ::std::fmt::Debug + Send + 'static,
            PC: TapesCreator<Item = T, Error = E>
        {
            let sample = T::make_sample(total);
            let mut check_table = Vec::new();
            for value in sample {
                check_table.push(value.clone());
                merge_tape.add(value).unwrap();
            }
            check_table
        }

        #[test]
        fn ntree_bkd_in_memory() {
            let mut merge_tape = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
            let check_table = fill_tape_and_check_table::<String, _, _>(&mut merge_tape, 1024);
            let (src, src_len) = merge_tape.finish().unwrap().unwrap();
            let ntree_writer = ntree_bkd::in_memory::InMemory::new();
            let reader_base = ntree_writer.build(src, src_len, 1, 128).unwrap();
            let mut reader_cursor = reader_base.cursor();
            for k in check_table.iter() {
                let mut iter = reader_cursor.lookup_iter(Some(k).into_iter());
                assert_eq!(iter.next().unwrap(), Some((k, k)));
                assert_eq!(iter.next().unwrap(), None);
            }
            {
                let mut iter = reader_cursor.lookup_iter(check_table.iter());
                let mut count = 0;
                while let Some((key, value)) = iter.next().unwrap() {
                    assert_eq!(key, value);
                    count += 1;
                }
                assert_eq!(count, check_table.len());
            }
            let mut iter = reader_cursor.lookup_iter(Some("some unexpected key").into_iter());
            assert_eq!(iter.next().unwrap(), None);
        }

        #[test]
        fn ntree_bkd_file() {
            let mut merge_tape = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
            let check_table = fill_tape_and_check_table::<String, _, _>(&mut merge_tape, 1024);
            let (src, src_len) = merge_tape.finish().unwrap().unwrap();
            let ntree_writer = ntree_bkd::file::FileWriter::new("/tmp/ntree_a").unwrap();
            ntree_writer.build::<_, ()>(src.map(|r| Ok(r.unwrap())), src_len, 1, 16).unwrap();
            let mut reader_cursor = ntree_bkd::file::FileReader::<String>::new("/tmp/ntree_a").unwrap();
            for k in check_table.iter() {
                let mut iter = reader_cursor.lookup_iter(Some(k).into_iter());
                assert_eq!(iter.next().unwrap(), Some((k, k)));
                assert_eq!(iter.next().unwrap(), None);
            }
            {
                let mut iter = reader_cursor.lookup_iter(check_table.iter());
                let mut count = 0;
                while let Some((key, value)) = iter.next().unwrap() {
                    assert_eq!(key, value);
                    count += 1;
                }
                assert_eq!(count, check_table.len());
            }
            let mut iter = reader_cursor.lookup_iter(Some("some unexpected key").into_iter());
            assert_eq!(iter.next().unwrap(), None);
        }

        #[test]
        fn ntree_bkd_mmap_true() {
            let mut merge_tape = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
            let check_table = fill_tape_and_check_table::<String, _, _>(&mut merge_tape, 1024);
            let (src, src_len) = merge_tape.finish().unwrap().unwrap();
            let ntree_writer = ntree_bkd::file::FileWriter::new("/tmp/ntree_b").unwrap();
            ntree_writer.build::<_, ()>(src.map(|r| Ok(r.unwrap())), src_len, 1, 16).unwrap();
            test_mmap_reader(
                ntree_bkd::mmap::MmapReader::new(
                    "/tmp/ntree_b",
                    ntree_bkd::mmap::MmapType::TrueMmap { madv_willneed: true, }).unwrap(),
                &check_table);
        }

        #[test]
        fn ntree_bkd_mmap_malloc() {
            let mut merge_tape = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
            let check_table = fill_tape_and_check_table::<String, _, _>(&mut merge_tape, 1024);
            let (src, src_len) = merge_tape.finish().unwrap().unwrap();
            let ntree_writer = ntree_bkd::file::FileWriter::new("/tmp/ntree_c").unwrap();
            ntree_writer.build::<_, ()>(src.map(|r| Ok(r.unwrap())), src_len, 1, 16).unwrap();
            test_mmap_reader(
                ntree_bkd::mmap::MmapReader::new("/tmp/ntree_c", ntree_bkd::mmap::MmapType::Malloc).unwrap(),
                &check_table);
        }

        fn test_mmap_reader<'s, T>(mut reader_cursor: ntree_bkd::mmap::MmapReader<T>, check_table: &Vec<T>)
            where T: ::serde::Deserialize<'s> + PartialOrd + PartialEq + ::std::fmt::Debug + ::std::borrow::Borrow<str>
        {
            for k in check_table.iter() {
                let mut iter = reader_cursor.lookup_iter(Some(k).into_iter());
                assert_eq!(iter.next().unwrap(), Some((k, k)));
                assert_eq!(iter.next().unwrap(), None);
            }
            {
                let mut iter = reader_cursor.lookup_iter(check_table.iter());
                let mut count = 0;
                while let Some((key, value)) = iter.next().unwrap() {
                    assert_eq!(key, value);
                    count += 1;
                }
                assert_eq!(count, check_table.len());
            }
            let mut iter = reader_cursor.lookup_iter(Some("some unexpected key").into_iter());
            assert_eq!(iter.next().unwrap(), None);
        }

        #[test]
        fn readme_examples() {
            let mut merge_tape = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
            let values = ["some value", "another value", "last example value"];
            for &value in values.iter() {
                merge_tape.add(value).unwrap()
            }
            let (src, src_len) = merge_tape.finish().unwrap().unwrap();
            let ntree_writer = ntree_bkd::in_memory::InMemory::new();
            let reader_tree = ntree_writer.build(src, src_len, 1, 16).unwrap();
            let mut reader_cursor = reader_tree.cursor();
            let mut lookup_iter = reader_cursor.lookup_iter(values.iter());
            while let Some((key, value)) = lookup_iter.next().unwrap() {
                assert_eq!(key, value)
            }
        }
    }

    mod mod_bench {
        use std::fmt::Debug;
        use super::Sampler;
        use super::test::{Bencher, black_box};
        use super::super::bkd::{in_memory, file_cache};
        use super::super::merge::{TapesCreator, BinMergeTape, ParallelConfig};

        fn bench_pile<T, E, TC, F>(b: &mut Bencher, total: usize, mut maker: F)
            where T: Sampler + PartialOrd,
                  E: Debug + Send + 'static,
                  TC: TapesCreator<Item = T, Error = E>,
                  F: FnMut() -> BinMergeTape<TC>
        {
            let sample = T::make_sample(total);
            b.iter(move || {
                let mut merge_tape = maker();
                for value in sample.iter().cloned() {
                    merge_tape.add(value).unwrap();
                }
                for value in merge_tape.finish().unwrap() {
                    black_box(value);
                }
            });
        }

        #[bench]
        fn bench_native_sort_i32(b: &mut Bencher) {
            let mut sample = i32::make_sample(512 * 1024);
            b.iter(|| { sample.sort(); });
        }

        #[bench]
        fn in_memory_bin_merge_tape_i32_singlethread(b: &mut Bencher) {
            bench_pile::<i32, _, _, _>(b, 512 * 1024, || BinMergeTape::new(in_memory::InMemoryTapesCreator::new()));
        }

        #[bench]
        fn in_memory_bin_merge_tape_i32_multithread(b: &mut Bencher) {
            bench_pile::<i32, _, _, _>(b, 512 * 1024, || BinMergeTape::with_params(
                in_memory::InMemoryTapesCreator::new(),
                ParallelConfig::MultiThread { max_slaves: 4, spawn_trigger_power: 14, }));
        }

        #[bench]
        fn bench_native_sort_str(b: &mut Bencher) {
            let mut sample = String::make_sample(128 * 1024);
            b.iter(|| { sample.sort(); });
        }

        #[bench]
        fn in_memory_bin_merge_tape_str_singlethread(b: &mut Bencher) {
            bench_pile::<String, _, _, _>(b, 128 * 1024, || BinMergeTape::new(in_memory::InMemoryTapesCreator::new()));
        }

        #[bench]
        fn in_memory_bin_merge_tape_str_multithread(b: &mut Bencher) {
            bench_pile::<String, _, _, _>(b, 128 * 1024, || BinMergeTape::with_params(
                in_memory::InMemoryTapesCreator::new(),
                ParallelConfig::MultiThread { max_slaves: 4, spawn_trigger_power: 14, }));
        }

        #[bench]
        fn file_cache_bin_merge_tape_str_singlethread(b: &mut Bencher) {
            bench_pile::<String, _, _, _>(
                b, 128 * 1024, || BinMergeTape::new(file_cache::FileCacheTapesCreator::new(14, "/tmp")));
        }

        #[bench]
        fn file_cache_bin_merge_tape_str_multithread(b: &mut Bencher) {
            bench_pile::<String, _, _, _>(b, 128 * 1024, || BinMergeTape::with_params(
                file_cache::FileCacheTapesCreator::new(14, "/tmp"),
                ParallelConfig::MultiThread { max_slaves: 4, spawn_trigger_power: 13, }));
        }
    }
}
