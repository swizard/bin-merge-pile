use std::sync::Arc;
use std::marker::PhantomData;
use std::path::{Path, PathBuf};
use super::merge::{WriteTape, TapesCreator};
use super::save_as::SaveAs;
use super::load_from::LoadFrom;

pub trait Reducer<T>: Send + Sync + 'static {
    type Error: Send + 'static;

    fn reduce(&self, existing: &mut T, incoming: T) -> Result<(), Self::Error>;
}

impl<T, E, F> Reducer<T> for F
    where F: Fn(&mut T, T) -> Result<(), E> + Send + Sync + 'static,
          E: Send + 'static
{
    type Error = E;

    fn reduce(&self, existing: &mut T, incoming: T) -> Result<(), Self::Error> {
        (*self)(existing, incoming)
    }
}

pub struct ReducerTape<Tape, R> where Tape: WriteTape {
    head: Option<Tape::Item>,
    tape: Tape,
    reducer: Arc<R>,
}

impl<Tape, R> ReducerTape<Tape, R> where Tape: WriteTape, R: Reducer<Tape::Item> {
    pub fn new(tape: Tape, reducer: Arc<R>) -> ReducerTape<Tape, R> {
        ReducerTape {
            head: None,
            tape: tape,
            reducer: reducer,
        }
    }
}

#[derive(Debug)]
pub enum Error<TE, RE> {
    WriteTape(TE),
    Reducer(RE),
}

pub struct ReadTapeMap<RT, RE> {
    tape: RT,
    _marker: PhantomData<RE>,
}

impl<RT, RE> ReadTapeMap<RT, RE> {
    fn new(tape: RT) -> ReadTapeMap<RT, RE> {
        ReadTapeMap {
            tape: tape,
            _marker: PhantomData,
        }
    }
}

impl<RT, T, E, RE> Iterator for ReadTapeMap<RT, RE> where RT: Iterator<Item = Result<T, E>> {
    type Item = Result<T, Error<E, RE>>;

    fn next(&mut self) -> Option<Self::Item> {
        self.tape.next().map(|r| r.map_err(Error::WriteTape))
    }
}

impl<RT, RE> SaveAs for ReadTapeMap<RT, RE> where RT: SaveAs {
    type Error = Error<RT::Error, RE>;

    fn save_as<P>(self, filename: P) -> Result<(), Self::Error> where P: AsRef<Path>, PathBuf: From<P> {
        self.tape.save_as(filename).map_err(Error::WriteTape)
    }
}

impl<RT, RE> LoadFrom for ReadTapeMap<RT, RE> where RT: LoadFrom {
    type Error = Error<RT::Error, RE>;

    fn load_from<P>(filename: P) -> Result<Self, Self::Error> where P: AsRef<Path>, PathBuf: From<P> {
        Ok(ReadTapeMap::new(LoadFrom::load_from(filename).map_err(Error::WriteTape)?))
    }
}

impl<T, E, F, Tape, R, RE> WriteTape for ReducerTape<Tape, R>
    where T: PartialOrd + Send + 'static,
          E: Send + 'static,
          F: Iterator<Item = Result<T, E>> + Send + 'static,
          Tape: WriteTape<Item = T, Error = E, ReadTape = F> + Send + 'static,
          R: Reducer<T, Error = RE> + Send + Sync + 'static,
          RE: Send + 'static
{
    type Item = Tape::Item;
    type Error = Error<Tape::Error, RE>;
    type ReadTape = ReadTapeMap<Tape::ReadTape, RE>;

    fn record(&mut self, value: Tape::Item) -> Result<(), Self::Error> {
        match &mut self.head {
            &mut Some(ref mut existing) if *existing == value =>
                self.reducer.reduce(existing, value).map_err(Error::Reducer),
            other => match ::std::mem::replace(other, Some(value)) {
                Some(prev_existing) => self.tape.record(prev_existing).map_err(Error::WriteTape),
                None => Ok(()),
            }
        }
    }

    fn stop(mut self) -> Result<Self::ReadTape, Self::Error> {
        if let Some(head) = self.head.take() {
            self.tape.record(head).map_err(Error::WriteTape)?;
        }

        Ok(ReadTapeMap::new(self.tape.stop().map_err(Error::WriteTape)?))
    }
}

pub struct ReducerTapesCreator<TC, R> {
    tapes_creator: TC,
    reducer: Arc<R>,
}

impl<T, E, F, Tape, TC, R> ReducerTapesCreator<TC, R>
    where T: PartialOrd + Send + 'static,
          E: Send + 'static,
          F: Iterator<Item = Result<T, E>> + Send + 'static,
          Tape: WriteTape<Item = T, Error = E, ReadTape = F> + Send + 'static,
          TC: TapesCreator<Item = T, Error = E, Tape = Tape>,
          R: Reducer<T> + Send + Sync + 'static
{
    pub fn new(tapes_creator: TC, reducer: R) -> ReducerTapesCreator<TC, R> {
        ReducerTapesCreator {
            tapes_creator: tapes_creator,
            reducer: Arc::new(reducer),
        }
    }
}

impl<T, E, F, Tape, TC, R, RE> TapesCreator for ReducerTapesCreator<TC, R>
    where T: PartialOrd + Send + 'static,
          E: Send + 'static,
          F: Iterator<Item = Result<T, E>> + Send + 'static,
          Tape: WriteTape<Item = T, Error = E, ReadTape = F> + Send + 'static,
          TC: TapesCreator<Item = T, Error = E, Tape = Tape>,
          R: Reducer<T, Error = RE> + Send + Sync + 'static,
          RE: Send + 'static
{
    type Item = T;
    type Error = Error<E, RE>;
    type Tape = ReducerTape<Tape, R>;

    fn make_tape(&mut self, max_len: usize) -> Result<ReducerTape<Tape, R>, Self::Error> {
        Ok(ReducerTape::new(
            self.tapes_creator.make_tape(max_len).map_err(Error::WriteTape)?,
            self.reducer.clone()))
    }
}
