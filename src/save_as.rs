use std::path::{Path, PathBuf};

pub trait SaveAs {
    type Error;

    fn save_as<P>(self, filename: P) -> Result<(), Self::Error> where P: AsRef<Path>, PathBuf: From<P>;
}
