# Binary merge pile crate #

## Summary ##

Sorting data structure designed for storage backends with fast sequential, but slow random access, such as HDD drives.

Typical usage pattern includes two separate stages: write-only (compile) and read-only (lookup).

### Compile stage ###

1. Sort and maybe aggregate data.
2. Build NTree lookup data structure.

```
#!rust
let mut merge_tape = BinMergeTape::new(in_memory::InMemoryTapesCreator::new());
let keys = ["some value", "another value", "last example value"];
for &key in keys.iter() {
    merge_tape.add(key).unwrap()
}
let (src, src_len) = merge_tape.finish().unwrap().unwrap();
let ntree_writer = ntree_bkd::in_memory::InMemory::new();
let reader_tree = ntree_writer.build(src, src_len, 1, 16).unwrap();
```

### Lookup state ###

Perform batch lookup (several keys at a time) in order to maximize sequential reads.

```
#!rust
let mut reader_cursor = reader_tree.cursor();
let mut lookup_iter = reader_cursor.lookup_iter(keys.iter());
while let Some((key, value)) = lookup_iter.next().unwrap() {
    assert_eq!(key, value)
}
```

## Using bin_merge_pile ##

To `Cargo.toml`:

```toml
[dependencies]
bin-merge-pile = "0.6"
```

To `src/main.rs`:

```
#!rust
extern crate bin_merge_pile;
```
